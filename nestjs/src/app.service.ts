import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(firstname: string, lastname: string): string {
    return `Hello, ${firstname} ${lastname}`;
  }
}
