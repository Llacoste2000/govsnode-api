import { Hono } from "hono";
const app = new Hono();

app.get("/", (c) => {
  const firstname = c.req.query("firstname");
  const lastname = c.req.query("lastname");
  return c.text(`Hello, ${firstname} ${lastname}`);
});

export default {
  port: 3000,
  fetch: app.fetch,
};
