# GOLANG (ECHO) VS NODE (NEST) API

The sole purpose of this repo is to know what are the differences of a GOLANG api vs a NODE api.
This will be achived with a load testing tool names `autocannon` (based on the command line `wrk` and `wrk2`).

## The environment

Both api are dockerised, because that's probably how production is deployed in most real worl cases, and they both have an equal amount of CPU ressource given (1 cpu) in their `docker-compose.yml` file.

## Purpose

The purpose is to precisely know the RAM each api use and how many Req/s they can produce with the same CPU power.
We already know that Node (Javascript / Typescript) API development is way easier / faster for the developper, and cheaper for the employer. There are already dozen of packages that can serve any needs, and even the Javascript basic langage comes with a lot of tools to facilitate the manipulation of data.
On the other hand Golang is compiled and is a lot lighter than a production Nodejs project, is multi-threaded and is more reliable for CPU intensive tasks (encryption, password salting, ...).
So the other purpose is, via this repo and by doing your own tests, to know if golang might just be overkill for your project =>

- Golang developper cost mode than a javascript back-end developper.
- One Javascript developper can actually take care of a fullstack project
- Transition between front-end javascript developper to a javascript back-end will be faster, and will maybe provide help

## Usage

### in _./golang_

`docker-compose build` : to build the docker image
`docker-compose up`: to launch the ECHO server with the 1 cpu limitation present in the `docker-compose.yml` file

navigate to the `./autocannon` folder and : `npm run test:golang`

### in _./node_

`docker-compose build` : to build the docker image
`docker-compose up`: to launch the NODE server with the 1 cpu limitation present in the `docker-compose.yml` file

navigate to the `./autocannon` folder and : `npm run test:node`
