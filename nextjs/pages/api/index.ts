// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<string>,
) {
  const firstname = req.query.firstname;
  const lastname = req.query.lastname;

  res.status(200).send(`Hello, ${firstname} ${lastname}`);
}
