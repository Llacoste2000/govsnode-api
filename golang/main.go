package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		firstName := c.QueryParam("firstname")
		lastName := c.QueryParam("lastname")

		return c.String(http.StatusOK, fmt.Sprintf("Hello, %v %v", firstName, lastName))
	})
	e.Logger.Fatal(e.Start(":8001"))
}
